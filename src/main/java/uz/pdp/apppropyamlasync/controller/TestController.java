package uz.pdp.apppropyamlasync.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/test")
public class TestController {

    @Value("${app.name:Uroq}")
    private String appName;

    @Value("${app.ports}")
    private List<String> ports;

    @Value("#{${app.address}}")
    private Map<String, Object> address;


    @GetMapping("/first")
    public String name() {
        return appName;
    }

    @GetMapping("/ports")
    public List<String> ports() {
        return ports;
    }

    @GetMapping("/address")
    public Map<String, Object> address() {
        return address;
    }
}
