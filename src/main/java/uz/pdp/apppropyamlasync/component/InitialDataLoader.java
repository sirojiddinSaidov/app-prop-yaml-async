package uz.pdp.apppropyamlasync.component;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import uz.pdp.apppropyamlasync.config.PropertiesToCustomClassConfig;
import uz.pdp.apppropyamlasync.entity.User;

@Component
@RequiredArgsConstructor
public class InitialDataLoader implements CommandLineRunner {

    @Value("${app.user.username}")
    private String username;

    @Value("${app.user.password}")
    private String password;

    @Value("${app.user.email:bla}")
    private String email;

    private final PropertiesToCustomClassConfig propertiesToCustomClassConfig;

    @Override
    public void run(String... args) throws Exception {
        System.out.println(User.builder()
                .username(username)
                .password(password)
                .email(email)
                .build());

        System.out.println(propertiesToCustomClassConfig.getUser());
        System.out.println(propertiesToCustomClassConfig.getUsers());


    }
}
