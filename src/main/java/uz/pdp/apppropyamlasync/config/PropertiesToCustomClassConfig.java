package uz.pdp.apppropyamlasync.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import uz.pdp.apppropyamlasync.entity.User;

import java.util.List;

@ConfigurationProperties(prefix = "app")
@Getter
@Setter
public class PropertiesToCustomClassConfig {

    private User user;

    private List<User> users;

}
