package uz.pdp.apppropyamlasync;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import uz.pdp.apppropyamlasync.config.PropertiesToCustomClassConfig;

@SpringBootApplication
@EnableConfigurationProperties(value = PropertiesToCustomClassConfig.class)
public class AppPropYamlAsyncApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppPropYamlAsyncApplication.class, args);
    }

}
